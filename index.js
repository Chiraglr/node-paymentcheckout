const express = require('express');
const axios = require('axios');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');
const model = require('./db.js');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cors());

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
  
app.post('/storeCustomer',(req,res)=>{
    model.findOne({firstName: {$eq: req.body.firstName}, lastName: {$eq: req.body.lastName}, email: {$eq: req.body.email}})
    .then(response=>{
        if(response!==null)
            res.send(response);
        else{
            model.insertOne(Object.assign(req.body,{merchantCustomerId: "1559900597600039485".split("").map((i)=>getRandomInt(1,9)).join("")})).then(response=>res.send(response.ops[0]));
        }
    });
})

app.get('/getCustomer/:customerId',(req,res)=>{
    axios({
        method: 'get',
        url: `https://api.test.paysafe.com/paymenthub/v1/customers?merchantCustomerId=mycustomer10`
    })
});

app.post('/createCustomer',(req,res)=>{
    axios({
        method: 'post',
        url: 'https://api.test.paysafe.com/paymenthub/v1/customers',
        auth: {
          username: 'private-7751',
          password: 'B-qa2-0-5f031cdd-0-302d0214496be84732a01f690268d3b8eb72e5b8ccf94e2202150085913117f2e1a8531505ee8ccfc8e98df3cf1748'
        },
        data: req.body
    }).then(response =>{
         model.updateOne({merchantCustomerId: req.body.merchantCustomerId},
            {$set: {customerId: response.data.id}})
            .then(res=>console.log("db ",`matchedCount: ${res.matchedCount}`,`modifiedCount: ${res.modifiedCount}`));
            res.json(response.data);
    },err=>{console.log(err);res.send(err)}).catch(err=>{console.log(err);res.send(err)});
});

app.post('/singleUseCustomerToken/:id',(req,res)=>{
    axios({
        method: 'post',
        url: `https://api.test.paysafe.com/paymenthub/v1/customers/${req.params.id}/singleusecustomertokens`,
        auth: {
          username: 'private-7751',
          password: 'B-qa2-0-5f031cdd-0-302d0214496be84732a01f690268d3b8eb72e5b8ccf94e2202150085913117f2e1a8531505ee8ccfc8e98df3cf1748'
        },
        data: req.body
      }).then(response => res.json(response.data),err=>{console.log(err);res.send(err)}).catch(err=>{console.log(err);res.send(err)});
});

app.post('/checkout',(req,res)=>{
    console.log(req.body.merchantRefNum);
    axios({
        method: 'post',
        // url: 'https://private-anon-8467725ed3-paysafeapipaymenthubv1.apiary-proxy.com/paymenthub/v1/payments',
        url: 'https://api.test.paysafe.com/paymenthub/v1/payments',
        auth: {
          username: 'private-7751',
          password: 'B-qa2-0-5f031cdd-0-302d0214496be84732a01f690268d3b8eb72e5b8ccf94e2202150085913117f2e1a8531505ee8ccfc8e98df3cf1748'
        },
        data: req.body
      }).then(function (response) {
            console.log("here",response.data);
            res.json(response.data);
        },err=>{console.log("erryup");res.send(err)}).catch(err=>{console.log("err",err);res.send(err)});
});

const port = process.env.PORT || 5000;
app.listen(port,'0.0.0.0',() => console.log(`Listening on port ${port}....`));