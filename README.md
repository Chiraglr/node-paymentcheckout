*Database: MongoDB hosted on mongoDB atlas   
*server: nodejs server hosted on ec2 nano instance   
   
## DataBase Schema:   
Fields:   
        * ticketNo,       int  (only 1-40 valid)    
        * status,         bool (true->closed, false->open)   
        * firstName,      string   
        * lastName,       string   
        * contactNo,      string   
        * streetAddress,  string   
        * city,           string    
        * state,          string   
        * pincode,        string   
        * landMark,       string   
   
## DataBase Admin:   
    * username: chirag   
    * password: qwerty123    