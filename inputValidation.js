
const { check, validationResult, body } = require('express-validator');

checkTicketNo = check('id','Invalid ticketNo').isInt({min:1, max: 40});

checkFirstName = body('firstName').custom((value, { req }) => {
    var regex = new RegExp("[a-zA-Z ]*");
    if(!regex.test(value) || value.length<1){
      throw new Error('invalid firstName');
    }
    return true;
});

checkLastName = body('lastName').custom((value, { req }) => {
    var regex = new RegExp("[a-zA-Z ]*");
    if(!regex.test(value) || value.length<1){
      throw new Error('invalid lastName');
    }
    return true;
});

checkStatus = body('status','invalid status').isBoolean();

checkContactNo = body('contactNo','Invalid contactNo').isMobilePhone('en-IN');

checkLandMark = body('landMark','Invalid landMark').isLength({ min: 4 });

checkState = body('state','Invalid State').isLength({ min: 5 });

checkPinCode = body('pincode','Invalid pincode').isNumeric({no_symbols: false});

checkStreetAddress = body('streetAddress','Invalid streetAddress').isLength({min: 10, max: 100});

checkUserName = body('username','Invalid username').isLength({min: 6});

checkPassword = body('password','Invalid password').isLength({min: 6});

module.exports = {
    checkTicketNo,
    checkFirstName,
    checkLastName,
    checkStatus,
    checkContactNo,
    checkLandMark,
    checkState,
    checkPinCode,
    checkStreetAddress,
    checkUserName,
    checkPassword
};